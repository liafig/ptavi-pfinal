#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""Class (annd main program) for a proxy."""

import socket
import socketserver
import sys
from datetime import datetime, date, time, timedelta
import json
import hashlib
from xml.sax import make_parser
from xml.sax.handler import ContentHandler

tags = {"server": ["name", "ip", "puerto"],
        "database": ["path", "passwdpath"],
        "log": ["path"]}


def log(path, caso, info, IP, PORT):
    """Write in log file."""
    log_file = open(path, "a")
    tiempo = datetime.now().strftime("%Y%m%d%H%M%S")
    if caso == "start":
        message = (tiempo + " Starting...\n")
    elif caso == "received":
        message = (tiempo + " Received from " + IP + ":" + str(PORT) + ": ")
        message += (info.replace("\r\n", " ").replace("\n", " ") + "\n")
    elif caso == "sent":
        message = (tiempo + " Sent to " + IP + ":" + str(PORT) + ": ")
        message += (info.replace("\r\n", " ").replace("\n", " ") + "\n")
    elif caso == "error":
        message = (tiempo + " Error: No server listening at " + IP + " port ")
        message += (str(PORT) + "\n")
    elif caso == "finish":
        message = (tiempo + " Finishing." + "\n")
    log_file.write(message)
    log_file.close()


def checknonce(user, server):
    """Nonce."""
    h_nonce = hashlib.sha224()
    h_nonce.update(bytes(user + server, "utf-8"))
    return h_nonce.hexdigest()


def passwordnonce(nonce, passwd):
    """Nonce of passwords."""
    h_nonce = hashlib.sha224()
    h_nonce.update(bytes(str(nonce) + str(passwd), "utf-8"))
    return h_nonce.hexdigest()


class XMLHandler(ContentHandler):
    """Class for the proxy."""

    def __init__(self, tags):
        """Function to pick init."""
        self.conf = {}
        self.tags = tags

    def startElement(self, name, attr):
        """Function to pick the attibutes."""
        if name in self.tags:
            for atrib in self.tags[name]:
                self.conf[name + "_" + atrib] = attr.get(atrib, "")

    def get_tags(self):
        """Function to get tags."""
        return self.conf


class PRRegisterHandler(socketserver.DatagramRequestHandler):
    """SIP Register server class."""

    dic = {}
    passwd = {}

    def expire(self):
        """Remove an user."""
        user_exp = []
        now = datetime.now().strftime("%Y%m%d%H%M%S")
        for user in self.dic:
            if now >= self.dic[user]["expire"]:
                user_exp.append(user)
        for user in user_exp:
            del self.dic[user]

    def register2json(self):
        """Convert dictionary to json."""
        with open("registered.json", "w") as jsonfile:
            json.dump(self.dic, jsonfile, indent=4)

    def json2registered(self):
        """Read a json file and use the info as a dictionary of users."""
        try:
            with open("registered.json", "r") as jsonfile:
                self.dic = json.load(jsonfile)
        except FileNotFoundError:
            pass

    def json2passwd(self):
        """Open a file and use the info as a dictionary of users and passwords."""
        try:
            with open("passwords.json", "r") as jsonfile:
                self.passwd = json.load(jsonfile)
        except FileNotFoundError:
            pass

    def socket_m(self, USER, info):
        """Send information through the socket."""
        try:
            with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
                ip = self.dic[USER]["ip"]
                port = int(self.dic[USER]["port"])
                my_socket.connect((ip, port))
                log(PATH, "sent", info, ip, port)
                my_socket.send(bytes(info, "utf-8") + b"\r\n")
                data = my_socket.recv(1024).decode("utf-8")
        except:
            data = ""

        return data

    def handle(self):
        """Handle method of the server class."""
        MESSAGE = ""
        if self.dic == {}:
            self.json2registered()

        if self.passwd == {}:
            self.json2passwd()

        C_IP = self.client_address[0]
        C_PORT = str(self.client_address[1])

        MESSAGE = self.rfile.read().decode("utf-8")
        log(PATH, "received", MESSAGE, C_IP, C_PORT)
        METHOD = MESSAGE.split()[0]

        if METHOD == "REGISTER" or METHOD == "register":
            USER = MESSAGE.split(":")[1]
            if USER in self.dic:
                print(USER + " has registered\r")
                expired = MESSAGE.split(":")[-1]
                if int(expired) == 0:
                    del self.dic[USER]
                else:
                    s_time = datetime.now() + timedelta(seconds=int(expired))
                    exp = s_time.strftime("%Y%m%d%H%M%S")
                    self.dic[USER]["expire"] = exp
                self.wfile.write(b"SIP/2.0 200 OK\r\n")
                log(PATH, "sent", "SIP/2.0 200 OK", C_IP, C_PORT)
            else:
                NAME = conf["server_name"]
                if "Authorization" in MESSAGE:
                    b_mess = MESSAGE.split('"')[1]
                    nonce = checknonce(USER, NAME)
                    passwd = self.passwd[USER]
                    pr_mess = passwordnonce(nonce, passwd)
                    if pr_mess == b_mess:
                        puerto = MESSAGE.split()[1].split(":")[2].split("S")[0]
                        t_seg = MESSAGE.split(":")[3].split()[0]
                        timexp = datetime.now() + timedelta(seconds=int(t_seg))
                        expired = timexp.strftime("%Y%m%d%H%M%S")
                        self.dic[USER] = {"ip": C_IP,
                                          "port": puerto,
                                          "expire": expired}
                        self.wfile.write(b"SIP/2.0 200 0K\r\n")
                        log(PATH, "sent", "SIP/2.0 200 OK", C_IP, C_PORT)
                    else:
                        info = "SIP/2.0 404 User Not Found\r\n"
                        self.wfile.write(bytes(info, "utf-8"))
                        log(PATH, "sent", info, C_IP, C_PORT)
                else:
                    nonce = checknonce(USER, NAME)
                    info = "SIP/2.0 401 Unathorized\r\n"
                    info += "WWW Authenticate: Digest nonce=" + '"'
                    info += nonce + '"\r\n'
                    self.wfile.write(bytes(info, "utf-8"))
                    log(PATH, "sent", info, C_IP, C_PORT)
        else:
            invited = MESSAGE.split(":")[1].split()[0]
            if METHOD == "INVITE" or METHOD == "invite":
                me = MESSAGE.split("\n")[3].split("=")[1].split()[0]
                print(me + " has invited " + invited + "\r")
                if invited in self.dic and me in self.dic:
                    data = self.socket_m(invited, MESSAGE)
                    self.wfile.write(bytes(data, "utf-8"))
                    log(PATH, "sent", data, C_IP, C_PORT)
                else:
                    info = "SIP/2.0 404 User Not Found\r\n"
                    self.wfile.write(bytes(info, "utf-8"))
                    log(PATH, "sent", info, C_IP, C_PORT)

            else:
                data = self.socket_m(invited, MESSAGE)
                self.wfile.write(bytes(data, "utf-8"))
                log(PATH, "sent", data, C_IP, C_PORT)

        self.register2json()
        self.expire()


if __name__ == "__main__":
    """
    Listens at localhost ('') port given
    and calls the PRRegisterHandler class to manage the request
    """
    try:
        CONFIG = sys.argv[1]
    except IndexError or ValueError:
        sys.exit("Usage: python3 proxy_registrar.py config")

    parser = make_parser()
    handler = XMLHandler(tags)
    parser.setContentHandler(handler)
    parser.parse(open(CONFIG))
    conf = handler.get_tags()

    SERVER = conf["server_ip"]
    PORT = int(conf["server_puerto"])
    PATH = conf["log_path"]

    serv = socketserver.UDPServer((SERVER, PORT), PRRegisterHandler)
    print("Server " + SERVER + " listening at port " + str(PORT) + "...")
    try:
        log(PATH, "start", None, None, None)
        serv.serve_forever()
    except KeyboardInterrupt:
        print(" Finalizado servidor")
        log(PATH, "finish", None, None, None)
