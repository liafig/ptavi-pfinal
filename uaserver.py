#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""Class (and main program) for a server."""

import socketserver
import socket
import os
import sys
from proxy_registrar import XMLHandler, log
from xml.sax import make_parser
from xml.sax.handler import ContentHandler

tags = {"account": ["username", "passwd"],
        "uaserver": ["ip", "puerto"],
        "rtpaudio": ["puerto"],
        "regproxy": ["ip", "puerto"],
        "log": ["path"],
        "audio": ["path"]}


class SHandler(socketserver.DatagramRequestHandler):
    """Class for the server."""

    audio = []

    def handle(self):
        """Function for the handle."""
        m_list = ["INVITE", "ACK", "BYE"]
        message = ""
        message = self.rfile.read().decode("utf-8")
        C_IP = self.client_address[0]
        C_PORT = self.client_address[1]
        log(PATH, "received", message, C_IP, C_PORT)
        METHOD = message.split()[0]

        if METHOD not in m_list:
            info = "SIP/2.0 405 Method Not Allowed\r\n\r\n"
            self.wfile.write(bytes(info, "utf-8"))
            log(PATH, "sent", info, C_IP, C_PORT)

        elif METHOD == m_list[0]:
            ip_au = message.split("\n")[3].split()[1]
            port_au = message.split("\n")[6].split()[1]
            self.audio.append(ip_au)
            self.audio.append(port_au)
            info = "SIP/2.0 100 Trying \r\n\r\n"
            info += "SIP/2.0 180 Ringing \r\n\r\n"
            info += "SIP/2.0 200 OK\r\n"
            info += "Content-Type: application/sdp\r\nv=0\r\n"
            info += "o=" + USER + " " + str(SERVER) + "\r\n" + "s=misesion\r\n"
            info += "t=0\r\n" + "m=audio " + str(PORT_AUDIO) + " RTP\r\n"
            self.wfile.write(bytes(info, "utf-8"))
            log(PATH, "sent", info, C_IP, C_PORT)

        elif METHOD == m_list[1]:
            audio = "./mp32rtp -i " + self.audio[0] + " -p " + self.audio[1]
            audio += " <" + AUDIO
            print("Vamos a ejecutar ", audio)
            os.system(audio)
            self.audio = []

        elif METHOD == m_list[2]:
            info = "SIP/2.0 200 OK\r\n"
            self.wfile.write(bytes(info, "utf-8"))
            log(PATH, "sent", info, C_IP, C_PORT)


if __name__ == "__main__":
    try:
        try:
            CONFIG = sys.argv[1]
        except IndexError or ValueError:
            sys.exit("Usage: python3 uaserver.py config")

        parser = make_parser()
        handler = XMLHandler(tags)
        parser.setContentHandler(handler)
        parser.parse(open(CONFIG))
        conf = handler.get_tags()

        USER = conf["account_username"]
        PATH = conf["log_path"]
        SERVER = conf["uaserver_ip"]
        PORT = int(conf["uaserver_puerto"])
        PR_SERVER = conf["regproxy_ip"]
        PR_PORT = int(conf["regproxy_puerto"])
        PORT_AUDIO = int(conf["rtpaudio_puerto"])
        AUDIO = conf["audio_path"]

        log(PATH, "start", None, None, None)
        serv = socketserver.UDPServer((SERVER, PORT), SHandler)
        print("Listening...")
        serv.serve_forever()
    except KeyboardInterrupt:
        log(PATH, "finish", None, None, None)
        print(" Finalizado servidor")
