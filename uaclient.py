#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""Client program that opens a socket into a server."""

import threading
import socket
import sys
import os
from xml.sax import make_parser
from xml.sax.handler import ContentHandler
from uaserver import tags
from proxy_registrar import log, passwordnonce, XMLHandler


class CMethods:
    """Class for the methods."""

    def __init__(self, CONFIG):
        """Init."""
        parser = make_parser()
        chandler = XMLHandler(tags)
        parser.setContentHandler(chandler)
        parser.parse(open(CONFIG))
        self.conf = chandler.get_tags()

    def register(self, OPTION, auth=""):
        """Register function."""
        USER = self.conf["account_username"]
        PORT = int(self.conf["uaserver_puerto"])
        info = "REGISTER sip:" + USER + ":" + str(PORT) + "SIP/2.0"
        info += "\r\n" + "Expires: " + OPTION
        if auth != "":
            info += "\r\nAuthorization: Digest response=" '"' + auth + '"'
        return info + "\r\n"

    def invite(self, OPTION):
        """Invite function."""
        USER = self.conf["account_username"]
        SERVER = self.conf["uaserver_ip"]
        PORT_AUDIO = str(self.conf["rtpaudio_puerto"])
        info = "INVITE sip:" + OPTION + " SIP/2.0\n"
        info += "Content-Type: application/sdp\r\n"
        info += "v=0\n"
        info += "o=" + USER + " " + SERVER + "\n"
        info += "s=misesion\n" + "t=0\n"
        info += "m=audio " + PORT_AUDIO + " RTP\r\n"
        return info + "\r\n"

    def ack(self, OPTION):
        """Ack function."""
        info = "ACK sip:" + OPTION + " SIP/2.0\r\n"
        return info + "\r\n"

    def bye(self, OPTION):
        """Bye function."""
        info = "BYE sip:" + OPTION + " SIP/2.0\r\n"
        return info + "\r\n"


def method_info(clase, metodo, op, dig=""):
    """For choosing the methods."""
    if metodo == "REGISTER" or metodo == "register":
        return clase.register(op, dig)
    elif metodo == "INVITE" or metodo == "invite":
        return clase.invite(op)
    elif metodo == "ACK" or metodo == "ack":
        return clase.ack(op)
    elif metodo == "BYE" or metodo == "bye":
        return clase.bye(op)


if __name__ == "__main__":

    try:
        CONFIG = sys.argv[1]
        METHOD = sys.argv[2]
        OPTION = sys.argv[3]
    except IndexError or ValueError:
        sys.exit("Usage: python3 uaclient.py config method option")

    xml = CMethods(CONFIG)
    PASSWORD = xml.conf["account_passwd"]
    PATH = xml.conf["log_path"]
    if xml.conf["regproxy_ip"] is None:
        PR_SERVER = "127.0.0.1"
    else:
        PR_SERVER = xml.conf["regproxy_ip"]
    PR_PORT = int(xml.conf["regproxy_puerto"])
    AUDIO = xml.conf["audio_path"]

    log(PATH, "start", None, None, None)

    with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
        my_socket.connect((PR_SERVER, PR_PORT))
        info = method_info(xml, METHOD, OPTION)
        log(PATH, "sent", info, PR_SERVER, PR_PORT)
        my_socket.send(bytes(info, "utf-8") + b"\r\n")
        try:
            data = my_socket.recv(1024).decode("utf-8")
            if data:
                print("Recibido -- ", data)
                log(PATH, "received", data, PR_SERVER, PR_PORT)
        except:
            data = ""
            log(PATH, "error", None, PR_SERVER, PR_PORT)

        if "Authenticate" in data:
            nonce = data.split('"')[1]
            h_nonce = passwordnonce(nonce, PASSWORD)
            info = method_info(xml, "REGISTER", OPTION, h_nonce)
            log(PATH, "sent", info, PR_SERVER, PR_PORT)
            my_socket.send(bytes(info, "utf-8") + b"\r\n")
            try:
                data = my_socket.recv(1024).decode("utf-8")
                log(PATH, "received", data, PR_SERVER, PR_PORT)
            except:
                data = ""
                log(PATH, "error", None, PR_SERVER, PR_PORT)

        elif "100" in data:
            mess = data.split("\r\n")
            r_ip = mess[7].split()[1]
            r_port = mess[10].split()[1]
            com = "./mp32rtp -i " + r_ip + " -p " + r_port + " <" + AUDIO
            info = method_info(xml, "ACK", OPTION)
            log(PATH, "sent", info, PR_SERVER, PR_PORT)
            my_socket.send(bytes(info, "utf-8") + b"\r\n")
            cvlc = "cvlc rtp://@" + r_ip + ":" + r_port
            print("Vamos a ejecutar ", cvlc)
            cvlc_h = threading.Thread(target=os.system(cvlc))
            cvlc_h.start()
            print("Vamos a ejecutar ", com)
            os.system(com)

        elif data == "":
                log(PATH, "error", None, PR_SERVER, PR_PORT)

    log(PATH, "finish", None, None, None)
    print("Terminando socket...")
